public class TopoApp {
    public static void main(String[] args) {
        Graph theGraph = new Graph();
        theGraph.addVertex('A'); // 0
        theGraph.addVertex('B'); // 1
        theGraph.addVertex('C'); // 2
        theGraph.addVertex('D'); // 3
        theGraph.addVertex('E'); // 4
        theGraph.addVertex('F'); // 5
        theGraph.addVertex('G'); // 6
        theGraph.addVertex('H'); // 7
        theGraph.addVertex('I'); // 8
        theGraph.addVertex('J'); // 9
        theGraph.addVertex('K'); // 10
        theGraph.addVertex('L'); // 11
        theGraph.addVertex('M'); // 12
        theGraph.addVertex('N'); // 13
        theGraph.addVertex('O'); // 14
        theGraph.addVertex('P'); // 15
        theGraph.addVertex('Q'); // 16
        theGraph.addVertex('R'); // 17
        theGraph.addVertex('S'); // 18
        theGraph.addVertex('T'); // 19
        theGraph.addEdge(3, 0); // DA
        theGraph.addEdge(4, 0); // EA
        theGraph.addEdge(4, 1); // EB
        theGraph.addEdge(5, 1); // FB
        theGraph.addEdge(5, 2); // FC
        theGraph.addEdge(6, 2); // GC
        theGraph.addEdge(7, 3); // HD
        theGraph.addEdge(8, 3); // ID
        theGraph.addEdge(8, 4); // JE
        theGraph.addEdge(9, 4); // KE
        theGraph.addEdge(9, 5); // KF
        theGraph.addEdge(10, 5); // LF
        theGraph.addEdge(10, 6); // LG
        theGraph.addEdge(11, 6); // MH
        theGraph.addEdge(12, 7); // NI
        theGraph.addEdge(13, 7); // OI
        theGraph.addEdge(13, 8); // JO
        theGraph.addEdge(14, 8); // KP
        theGraph.addEdge(14, 9); // KQ
        theGraph.addEdge(15, 9); // LQ
        theGraph.addEdge(15, 10); // LR
        theGraph.addEdge(16, 10); // MR
        theGraph.addEdge(17, 11); // SM
        theGraph.addEdge(17, 12); // SN
        theGraph.addEdge(18, 12); // OT
        theGraph.addEdge(18, 13); // OU
        theGraph.addEdge(19, 13); // PV
        theGraph.addEdge(0, 14); // AP
        theGraph.topo(); // perform the sort
    } // end main()
} // end class TopoApp