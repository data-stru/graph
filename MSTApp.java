public class MSTApp {
    public static void main(String[] args) {
        Graph theGraph = new Graph();
        theGraph.addVertex('A'); // 0 (start for mst)
        theGraph.addVertex('B'); // 1
        theGraph.addVertex('C'); // 2
        theGraph.addVertex('D'); // 3
        theGraph.addVertex('E'); // 4
        theGraph.addVertex('F'); // 5
        theGraph.addVertex('G'); // 6
        theGraph.addVertex('H'); // 7
        theGraph.addVertex('I'); // 8
        theGraph.addVertex('J'); // 9
        theGraph.addVertex('K'); // 10
        theGraph.addVertex('L'); // 11
        theGraph.addVertex('M'); // 12
        theGraph.addVertex('N'); // 13
        theGraph.addVertex('O'); // 14
        theGraph.addVertex('P'); // 15
        theGraph.addVertex('Q'); // 16
        theGraph.addVertex('R'); // 17
        theGraph.addVertex('S'); // 18
        theGraph.addVertex('T'); // 19
        theGraph.addEdge(0, 1);
        theGraph.addEdge(0, 2);
        theGraph.addEdge(0, 3);
        theGraph.addEdge(1, 4);
        theGraph.addEdge(1, 5);
        theGraph.addEdge(2, 6);
        theGraph.addEdge(2, 7);
        theGraph.addEdge(3, 8);
        theGraph.addEdge(3, 9);
        theGraph.addEdge(4, 10);
        theGraph.addEdge(4, 11);
        theGraph.addEdge(5, 12);
        theGraph.addEdge(5, 13);
        theGraph.addEdge(6, 14);
        theGraph.addEdge(6, 15);
        theGraph.addEdge(7, 16);
        theGraph.addEdge(7, 17);
        theGraph.addEdge(8, 18);
        theGraph.addEdge(8, 19);
        theGraph.addEdge(9, 10);
        theGraph.addEdge(11, 12);
        theGraph.addEdge(13, 14);
        theGraph.addEdge(15, 16);
        theGraph.addEdge(17, 18);
        System.out.print("Minimum spanning tree: ");
        theGraph.mst(); // minimum spanning tree
        System.out.println();
    } // end main()
} // end class MSTApp